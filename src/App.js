import "./App.css";
import Login from "./components/Login";
import Backlog from "./components/Backlog";
import Home from "./components/Home";
import Projects from "./components/Projects";
import ProtectedRoute from "./components/ProtectedRoute";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { AuthProvider } from "./hooks/useAuth";
import RegisterUser from "./components/RegisterUser";
import Profile from "./components/Profile";

function App() {
  return (
    <div className="root">
      <AuthProvider>
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route
              path="/projects"
              element={
                <ProtectedRoute>
                  <Projects />
                </ProtectedRoute>
              }
            />
            <Route
              path="/projects/:id/backlog"
              element={
                <ProtectedRoute>
                  <Backlog />
                </ProtectedRoute>
              }
            />
            <Route
              path="/profile"
              element={
                <ProtectedRoute>
                  <Profile />
                </ProtectedRoute>
              }
            />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<RegisterUser />} />
          </Routes>
        </Router>
      </AuthProvider>
    </div>
  );
}

export default App;
