import { useState, createContext, useContext } from "react";

const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({ children }) {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [accessToken, setAccessToken] = useState(null);
  const [error, setError] = useState(null);

  const login = async (email, password) => {
    try {
      const response = await fetch("http://localhost:3000/api/users/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, password }),
      });

      if (response.ok) {
        const data = await response.json();
        setAccessToken(data);
        setIsAuthenticated(true);
        return true;
      } else {
        setError("Login failed: " + response.statusText);
        return false;
      }
    } catch (e) {
      setError(e);
      return false;
    }
  };

  const logout = () => {
    setIsAuthenticated(false);
    setAccessToken(null);
  };

  return (
    <AuthContext.Provider
      value={{ isAuthenticated, accessToken, login, logout, error }}
    >
      {children}
    </AuthContext.Provider>
  );
}
