import React, { useState } from "react";
import styles from "./index.module.css";
import Navbar from "../Navbar";
import { useParams } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";
import GetData from "../../requests/GetData";
import TaskList from "../TaskList";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

import "bootstrap/dist/css/bootstrap.min.css";

const Backlog = () => {
  const { id } = useParams();
  const { accessToken } = useAuth();
  const [data, loading, error] = GetData(
    `http://localhost:3000/api/projects/${id}/sprints`,
    accessToken
  );

  const [show, setShow] = useState(false);
  const [name, setName] = useState("");
  const [goal, setGoal] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [success, setSuccess] = useState(null);
  const [postLoading, setPostLoading] = useState(false);
  const [postError, setPostError] = useState(null);

  const handleClose = () => setShow(false);

  const handleCreate = async (e) => {
    e.preventDefault();
    setPostLoading(true);
    try {
      const response = await fetch(`http://localhost:3000/api/${id}/sprints`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
          "Access-Control-Allow-Origin": "http://localhost:3006",
        },
        body: JSON.stringify({ name, startDate, endDate, goal, projectId: id }),
      });

      if (!response.ok) {
        throw new Error(`HTTP error: Status ${response.status}`);
      }

      setSuccess(true);
      setPostError(null);
      handleClose();
    } catch (err) {
      setPostError(err.message);
      setSuccess(false);
    } finally {
      setPostLoading(false);
    }
  };

  return (
    <>
      <div>
        <Navbar />
      </div>
      <div>
        {loading && <div className={styles.loader}>Loading posts...</div>}
        {error && <div className={styles.error}>{error}</div>}
        <Accordion defaultActiveKey={["0"]} alwaysOpen>
          {data &&
            data.map((sprint) => (
              <Accordion.Item eventKey={sprint?.id}>
                <Accordion.Header className={styles.name}>
                  {sprint.name}
                </Accordion.Header>
                <Accordion.Body>
                  <TaskList projectId={id} sprintId={sprint.id} />
                </Accordion.Body>
              </Accordion.Item>
            ))}
        </Accordion>
        <Button variant="primary" size="lg" onClick={() => setShow(true)}>
          Create Sprint
        </Button>
        <Modal show={show} onHide={handleClose}>
          {postError && <div className={styles.error}>{postError}</div>}
          <Modal.Header closeButton>
            <Modal.Title>Create Project</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Sprint Name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlTextarea1"
              >
                <Form.Label>Goal</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  value={goal}
                  onChange={(e) => setGoal(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlDate1">
                <Form.Label>Start Date</Form.Label>
                <Form.Control
                  type="date"
                  rows={3}
                  value={startDate}
                  onChange={(e) => setStartDate(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlDate2">
                <Form.Label>End Date</Form.Label>
                <Form.Control
                  type="date"
                  rows={3}
                  value={endDate}
                  onChange={(e) => setEndDate(e.target.value)}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="primary"
              onClick={handleCreate}
              disabled={postLoading}
            >
              {postLoading ? "Saving..." : "Save Changes"}
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </>
  );
};

export default Backlog;
