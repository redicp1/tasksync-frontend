import React, { useState } from "react";
import styles from "./index.module.css";
import Navbar from "../Navbar";
import { useAuth } from "../../hooks/useAuth";
import GetData from "../../requests/GetData";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";

const Projects = () => {
  const { accessToken } = useAuth();
  const [data, loading, error] = GetData(
    `http://localhost:3000/api/users/${accessToken}/ownedprojects`,
    accessToken
  );

  const [show, setShow] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [success, setSuccess] = useState(null);
  const [postLoading, setPostLoading] = useState(false);
  const [postError, setPostError] = useState(null);

  const handleClose = () => setShow(false);

  const handleCreate = async (e) => {
    e.preventDefault();
    setPostLoading(true);
    try {
      const response = await fetch(
        `http://localhost:3000/api/${accessToken}/projects`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
            "Access-Control-Allow-Origin": "http://localhost:3006",
          },
          body: JSON.stringify({ name, description }),
        }
      );

      if (!response.ok) {
        throw new Error(`HTTP error: Status ${response.status}`);
      }

      setSuccess(true);
      setPostError(null);
      handleClose();
    } catch (err) {
      setPostError(err.message);
      setSuccess(false);
    } finally {
      setPostLoading(false);
    }
  };

  return (
    <>
      <div>
        <Navbar />
      </div>
      <div>
        {loading && <div className={styles.loader}>Loading posts...</div>}
        {error && <div className={styles.error}>{error}</div>}
        <div className={styles.container}>
          <h1 className={styles.title}>Project List</h1>
          <ul className={styles.list}>
            {data &&
              data.map((project) => (
                <Link
                  key={project.id}
                  to={`/projects/${project.id}/backlog`}
                  style={{ textDecoration: "none", color: "black" }}
                >
                  <li className={styles.item}>
                    <h2>{project.name}</h2>
                    <p>{project.description}</p>
                    <p>Created: {new Date(project.created).toLocaleString()}</p>
                  </li>
                </Link>
              ))}
            <div className="mb-2">
              <Button variant="primary" size="lg" onClick={() => setShow(true)}>
                Create Project
              </Button>
              <Modal show={show} onHide={handleClose}>
                {postError && <div className={styles.error}>{postError}</div>}
                <Modal.Header closeButton>
                  <Modal.Title>Create Project</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Form>
                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlInput1"
                    >
                      <Form.Label>Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Project Name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                      />
                    </Form.Group>
                    <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlTextarea1"
                    >
                      <Form.Label>Description</Form.Label>
                      <Form.Control
                        as="textarea"
                        rows={3}
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                      />
                    </Form.Group>
                  </Form>
                </Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="primary"
                    onClick={handleCreate}
                    disabled={postLoading}
                  >
                    {postLoading ? "Saving..." : "Save Changes"}
                  </Button>
                </Modal.Footer>
              </Modal>
            </div>
          </ul>
        </div>
      </div>
    </>
  );
};

export default Projects;
