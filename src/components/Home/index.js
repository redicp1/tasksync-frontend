import React from "react";
import styles from "./index.module.css";
import Navbar from "../Navbar";

const Home = () => {
  return (
    <>
      <div>
        <Navbar />
      </div>
      <div>
        <div className={styles.content}>Home</div>;
      </div>
    </>
  );
};

export default Home;
