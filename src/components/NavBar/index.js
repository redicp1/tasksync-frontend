import React from "react";
import { Link } from "react-router-dom";
import styles from "./index.module.css";
import { useAuth } from "../../hooks/useAuth";

const Navbar = () => {
  const { isAuthenticated, logout } = useAuth();

  return (
    <ul className={styles.navList}>
      {isAuthenticated ? (
        <>
          <li onClick={logout}>Log out</li>
          <Link to="/profile">
            <li>Profile</li>
          </Link>
          <Link to="/projects">
            <li>Projects</li>
          </Link>
          <Link to="/">
            <li>Home</li>
          </Link>
        </>
      ) : (
        <Link to="/login">
          <li>Login</li>
        </Link>
      )}
    </ul>
  );
};

export default Navbar;
