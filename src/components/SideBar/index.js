import React from "react";
import { Link } from "react-router-dom";
import styles from "./index.module.css";

const Sidebar = () => {
  return (
    <div>
      <div className={styles.sidebar}>
        <Link to="/backlog">
          <a className={styles.active}>Backlog</a>
        </Link>
        <Link to="/board">
          <a>Board</a>
        </Link>
      </div>
    </div>
  );
};

export default Sidebar;
