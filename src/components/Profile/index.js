import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import GetData from "../../requests/GetData";
import { useAuth } from "../../hooks/useAuth";
import Navbar from "../Navbar";
import { useNavigate } from "react-router-dom";

import styles from "./index.module.css";

const Profile = () => {
  const { accessToken, logout } = useAuth();
  const [data, error] = GetData(
    `http://localhost:3000/api/users/${accessToken}/profile`,
    accessToken
  );
  const navigate = useNavigate();
  const [username, setUsername] = useState(data?.username);
  const [email, setEmail] = useState(data?.email);
  const [password, setPassword] = useState("");
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    setUsername(data?.username);
    setEmail(data?.email);
  }, [data]);

  const [postLoading, setPostLoading] = useState(false);
  const [postError, setPostError] = useState(null);

  const [deleteLoading, setDeleteLoading] = useState(false);
  const [deleteError, setDeleteError] = useState(null);

  const handleUpdate = async (e) => {
    e.preventDefault();
    setPostLoading(true);
    try {
      const response = await fetch(
        `http://localhost:3000/api/users/${accessToken}/update`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
            "Access-Control-Allow-Origin": "http://localhost:3006",
          },
          body: JSON.stringify({ username, email, password }),
        }
      );

      if (!response.ok) {
        throw new Error(`HTTP error: Status ${response.status}`);
      }

      setSuccess(true);
      setPostError(null);
    } catch (err) {
      setSuccess(false);
      setPostError(err.message);
    } finally {
      setPostLoading(false);
    }
  };

  const handleDelete = async (e) => {
    e.preventDefault();
    setDeleteLoading(true);
    try {
      const response = await fetch(
        `http://localhost:3000/api/users/${accessToken}/delete`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
            "Access-Control-Allow-Origin": "http://localhost:3006",
          },
        }
      );

      if (!response.ok) {
        throw new Error(`HTTP error: Status ${response.status}`);
      }

      setDeleteError(null);
      logout();
      navigate("/");
    } catch (err) {
      setDeleteError(err.message);
    } finally {
      setDeleteLoading(false);
    }
  };

  return (
    <>
      <div>
        <Navbar />
      </div>
      <div className={styles.container}>
        {error ??
          postError ??
          (deleteError && (
            <div className={styles.error}>
              {postError ?? deleteError ?? error}
            </div>
          ))}
        {success && <div className={styles.success}>Successfully updated</div>}
        <Form className={styles.form}>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="text"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput3">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>
          <div>
            <Button
              variant="primary"
              onClick={handleUpdate}
              className={styles.button}
              disabled={postLoading}
            >
              {postLoading ? "Saving..." : "Update Profile"}
            </Button>
            <Button
              variant="danger"
              onClick={handleDelete}
              className={styles.button}
              disabled={deleteLoading}
            >
              {deleteLoading ? "Deleting..." : "Delete Profile"}
            </Button>
          </div>
        </Form>
      </div>
    </>
  );
};

export default Profile;
