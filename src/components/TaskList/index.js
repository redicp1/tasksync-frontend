import React from "react";
import styles from "./index.module.css";
import { useAuth } from "../../hooks/useAuth";
import GetData from "../../requests/GetData";
import Badge from "react-bootstrap/Badge";
import ListGroup from "react-bootstrap/ListGroup";
import "bootstrap/dist/css/bootstrap.min.css";

const TaskList = ({ projectId, sprintId }) => {
  const { accessToken } = useAuth();
  const [data, loading, error] = GetData(
    `http://localhost:3000/api/${projectId}/sprints/${sprintId}/tasks`,
    accessToken
  );

  return (
    <div className={styles.dataList}>
      {loading && <div className={styles.loader}>Loading posts...</div>}
      {error && <div className={styles.error}>{error}</div>}
      <ListGroup as="ul">
        {data &&
          data.map((task) => (
            <ListGroup.Item
              as="li"
              className="d-flex justify-content-between align-items-start"
              key={task.id}
            >
              <div className="ms-2 me-auto">
                <div>{task.title}</div>
              </div>
              <Badge bg="primary" pill>
                Assignee
              </Badge>
            </ListGroup.Item>
          ))}
      </ListGroup>
    </div>
  );
};

export default TaskList;
