# TaskSync Backend

The repository contains frontend part of the TaskSync: Collaborative Task
Manager.

## Software Requirement

In order to run the TaskSync project in development mode on Linux, macOS or
Windows, the following must be installed.

- [NodeJS 18][nodejs]

## Getting started

Clone and setup the project.

> Note: your CLI should have elevated privileges when setting up and starting
> the app

```sh
git clone https://git.fhict.nl/I464936/tasksync-frontend.git
npm install
```

The project can be served using the following command.

```sh
npm start
```

# CI/CD

The structure of the pipeline can be found in the "gitlab-ci.yaml" and contains
the following steps:

1. build stage
2. deployment of the images to DockerHub Registry

[nodejs]: https://nodejs.org
